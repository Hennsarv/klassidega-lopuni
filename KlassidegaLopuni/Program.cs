﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidegaLopuni
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom l = new Metsloom("ämblik", 8);
            Loom k = new Metsloom("krokodill");
 
            //Loom t = new Loom();

            Koduloom lh = new Koduloom("lehm") { Tõug = "briisi", Nimi = "Kirjak" };

            Koer pallu = new Koer() { Nimi = "Pallu", Tõug = "taksi" };
            Kass miis = new Kass() { Nimi = "Miisu", Tõug = "siiami" };
            Kass juula = new Kass() { Nimi = "Juula", Tõug = "pärslane" };

            pallu.Sööb(miis);
            miis.Sööb(pallu);

            juula.Sööb(miis);


            miis.TeebHäält();
            juula.TeebHäält();

            juula.Silita();
            juula.TeebHäält();

            k.Sööb(miis);

            foreach (var x in Loom.Loomaaed) Console.WriteLine(x);
            // ära siia taha midagi kirjuta

            Sepik s = new Sepik();
            s.Söömine();

            Lõuna(miis);
            Lõuna(k);
            Lõuna(s);
        }

        static void Lõuna(object x)
        {
            // sööme asja vaid kui ta on ISöödav
            if (x is ISöödav) ((ISöödav)x).Söömine();
            else Console.WriteLine("täna jääme nälga");

            // kasutame operatsioon as
            var v = x as ISöödav;
            var w = x is ISöödav ? (ISöödav)x : null;

            // tinglik meetodi väljakutse
            v?.Söömine();
            if (v != null) v.Söömine();

        }

    }

    // abstraktne loom - muutuja võib olla Loom
    // aga abstraktset looma teha ei saa
    abstract class Loom
    {
        public static List<Loom> Loomaaed = new List<Loom>();

        public readonly string Liik;
        protected int JalgadeArv;  // see on näha tuletatud klassis
                                   // aga ei ole näha mujal

        public Loom(string liik)
        {
            Loomaaed.Add(this);
            Liik = liik;
        }

        public abstract void TeebHäält();

        public virtual void Sööb(Loom teine)
        {
            Console.WriteLine($"{this} sööb {teine}");
        }

        public Loom() : this("tundmatu")
        {
            JalgadeArv = 3;
        }

        public override string ToString()
        {
            return $"{Liik} tal on {JalgadeArv} jalga";
        }
    }

    // Metsloom peab implementeerima (overridima) abstraktse baasklassi
    // kõik abstraktsed meetodid - Teeb Häält
    class Metsloom : Loom
    {

        public Metsloom(string liik) : base(liik) { }

        public Metsloom(string liik, int jalgadearv) : base(liik) 
        {
            JalgadeArv = jalgadearv;
            // siin näeme JalgadeArvu, 
            // kuna see on protected mitte private
        }

        public override void TeebHäält()
        {
            Console.WriteLine($"{Liik} teeb koledat häält");
        }
    }

    class Koduloom : Loom
    {
        public string Tõug;
        public string Nimi;

        public Koduloom(string liik) : base(liik)
        {
            JalgadeArv = 4;
        }

        public override string ToString()
        {
            return $"{Tõug} tõugu {Liik} nimega {Nimi}";
        }

        public override void Sööb(Loom teine)
        {
            if (this.GetType() == teine.GetType() )
                Console.WriteLine( this.GetType().Name + " teist samasugust ei söö ");
            else
            base.Sööb(teine);
        }

        public override void TeebHäält()
        {
            throw new NotImplementedException();
        }
    }

    class Koer : Koduloom
    {
        public Koer() : base("koer") { }

        public override void TeebHäält()
        {
            Console.WriteLine($"Koer {Nimi} haugub");
        }

    }
    class Kass : Koduloom, ISöödav
    {
        private bool Tuju = false;
        public Kass() : base("kass") { }

        public void Silita() { Tuju = true; }


        public override void Sööb(Loom teine)
        {
            if (teine is Koduloom) Console.WriteLine("kass koduloomi ei söö");
            else base.Sööb(teine);
        }

        public void Söömine()
        {
            Console.WriteLine($"kass {Nimi} pistetakse nahka"); 
        }

        public override void TeebHäält()
        {
            if (Tuju)
                Console.WriteLine($"kassike {Nimi} lööb nurru");
            else
            Console.WriteLine($"kass {Nimi} näugub"); 
        }

    }

    interface ISöödav
    {
        void Söömine();
    }


    class Sepik : ISöödav
    {
        public void Söömine()
        {
            Console.WriteLine("keegi nosib sepikut");
        }
    }




}
